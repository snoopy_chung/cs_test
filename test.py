#lgorithm FaceRecognition -compare target.png  aaa.jpg
#Create gallery: br -algorithm FaceRecognition -enrollAll -enroll  project/  'user.gal'
from brpy import init_brpy
import cv2
import time
cap = cv2.VideoCapture(0)


#openbr setting
br = init_brpy(br_loc='/usr/local/lib')
br.br_initialize_default()
br.br_set_property('algorithm','FaceRecognition') 

datagal=br.br_make_gallery('user.gal')
datagal_load = br.br_load_from_gallery(datagal)
datagal_nqueries = br.br_num_templates(datagal_load)

scores = []

target_img = open('target.png', 'rb').read() 
target_load = br.br_load_img(target_img, len(target_img))
target_query = br.br_enroll_template(target_load)
target_nqueries = br.br_num_templates(target_query)
scoresmat = br.br_compare_template_lists(target_query, datagal_load)
for c in range(datagal_nqueries):
	scores.append(("test", br.br_get_matrix_output_at(scoresmat, 0, c)))

#time.sleep(2)

br.br_free_template(target_load)
br.br_free_template_list(target_query)

# print top 10 match URLs
#scores.sort(key=lambda s: s[1])
#for s in scores[:10]:
#    print(s[0])
print scores
# clean up - no memory leaks
#br.br_free_template(datagal_load)
#br.br_free_template_list(data_query)
br.br_finalize()
